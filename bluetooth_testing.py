import asyncio
from time import sleep
from bleak import BleakClient, BleakGATTCharacteristic

ADDRESS = "74:bf:c0:04:31:3f"
CONNECTION_NICKNAME_WRITE_UUID = "00010006000010000000d8492fffa821"
GPS_FROM_MOBILE_UUID = "00040003000010000000d8492fffa821"
GPS_UPDATE_ATTRIBUTE = "00040002000010000000d8492fffa821"
PAIRING_ACCEPT_NOTIFY = "0001000a000010000000d8492fffa821"

ORIGINAL_GPS = "044ef9c9574257020a843f2b0000000000000000"

async def main(address):
    async with BleakClient(address) as client:
        ready = False

        async def accepted_callback(sender: BleakGATTCharacteristic, data: bytearray):
            await client.write_gatt_char(PAIRING_ACCEPT_NOTIFY, bytes.fromhex("01"), response=True)

        async def gps_changed(sender: BleakGATTCharacteristic, data: bytearray):
            global ready
            print("I think you have changed GPS status.")
            print("Response value", data)

            await client.write_gatt_char("", bytes.fromhex("01"), response=True)
            ready = True
        
        if not client.is_connected:
            value = await client.write_gatt_char(CONNECTION_NICKNAME_WRITE_UUID, bytes.fromhex("01") + b"Python", response=True)
            print(f"Response value:",value)
            await client.start_notify(CONNECTION_NICKNAME_WRITE_UUID, accepted_callback)
            await client.start_notify(GPS_FROM_MOBILE_UUID, gps_changed)
        
        # Check if GPS via Mobile is enabled
        if int.from_bytes(await client.read_gatt_char(GPS_FROM_MOBILE_UUID)) == 1:
            ready = True

        while not ready:
            await asyncio.sleep(2)
        
        running = True
        while running:
            try:
                degrees = float(input("Input degrees: "))
                minutes = float(input("Input minutes: "))
                seconds = float(input ("Input seconds: "))

                decimal_degrees = degrees + minutes/60 + seconds/3600

                order_of_magnitude = 0 # Starts with 0.1 seconds
                while 128 / (4**(10-order_of_magnitude)) <= decimal_degrees:
                    order_of_magnitude += 1
                
                order_of_magnitude_hex = str(hex(0x38 + order_of_magnitude))[2:]
                decimal_order_magnitude = 128 / (4**(11-order_of_magnitude))
                decimal_degrees -= decimal_order_magnitude

                first_nybble_value = decimal_order_magnitude / 8
                nybbles = [0,0,0,0,0,0]
                i_double = 1  # If the first nybble is greater than 8, all subsequent bits are worth double their value
                
                for i in range(6):
                    nybble_value = (first_nybble_value / 16 ** i) * (i_double)
                    if i == 0 and int(decimal_degrees // nybble_value) > 8:
                        nybble_0_value = 8
                        i_double = 2
                        decimal_degrees -= 8 * nybble_value
        
                        nybble_value *= 2
                        while decimal_degrees - nybble_value >= 0:
                            nybble_0_value += 1
                            decimal_degrees -= nybble_value
                        
                        nybbles[i] = str(hex(nybble_0_value))[2:]
                    else:
                        nybbles[i] = str(hex(int(decimal_degrees // nybble_value)))[2:]
                        decimal_degrees = decimal_degrees % nybble_value

                # Re-organize nybbles
                new_value = nybbles[4] + nybbles[5] + nybbles[2] + nybbles[3] + nybbles[0] + nybbles[1] + order_of_magnitude_hex
                print(new_value)

                change = 14 #int(input("What nybble to change ?"))
                await client.write_gatt_char("00040002000010000000d8492fffa821", bytes.fromhex(ORIGINAL_GPS[:change]+new_value+ORIGINAL_GPS[change+8:]), response=True)

            except KeyboardInterrupt:
                running = False
        

asyncio.run(main(ADDRESS))
